# RandomID generator
Simple tool for generating random IDs.

## Installation
To use the generator, install package by running the following command.
```
npm i npm i @krzysieq/randomid
```

## Usage
```javascript
const randomId = require('@krzysieq/randomid');
const id = randomId(10);

console.log(id); // KjmILIqHbn
```

## License
This project is licensed under the MIT License.